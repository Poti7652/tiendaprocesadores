package com.example.tienda_procesadores.Services;

import com.example.tienda_procesadores.Models.Peticion_DetalleProcesadores;
import com.example.tienda_procesadores.Models.Peticion_Login;
import com.example.tienda_procesadores.Models.Peticion_Registro;
import com.example.tienda_procesadores.Models.Peticion_Procesadores;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioAPI {

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> IniciarSesion(@Field("username") String name, @Field("password") String pass);


    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Peticion_Registro> Registrase(@Field("username") String name, @Field("password") String pass);


    @FormUrlEncoded
    @POST("api/informacionProcesadores")
    Call<Peticion_Procesadores> TodosLosProcesadores(@Field("") String s);


    @FormUrlEncoded
    @POST("api/detalleProcesador")
    Call<Peticion_Procesadores> DetallesProcesador(@Field("procesadorId") int s);

}
