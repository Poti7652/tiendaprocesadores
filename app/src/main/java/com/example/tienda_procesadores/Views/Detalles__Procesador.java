package com.example.tienda_procesadores.Views;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tienda_procesadores.API.API;
import com.example.tienda_procesadores.Models.Peticion_DetalleProcesadores;
import com.example.tienda_procesadores.Models.Peticion_Procesadores;
import com.example.tienda_procesadores.R;
import com.example.tienda_procesadores.Services.ServicioAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detalles__Procesador extends AppCompatActivity {
    TextView nombre, nucleos, frecuencia, ID;
    ImageButton btnRegresar;
    Peticion_Procesadores.Procesador procesador;
    String nom, nucl, fre;
    ServicioAPI servicio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_procesador);
        servicio = API.getApi(this).create(ServicioAPI.class);
        ID = findViewById(R.id.txtvId);
        nombre = findViewById(R.id.txtvnombre);
        nucleos = findViewById(R.id.txtvNucleos);
        frecuencia = findViewById(R.id.txtvFrecuencia);
        btnRegresar = findViewById(R.id.ibtnAtras);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Detalles__Procesador.this, Dashboard.class));
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            int Id = bundle.getInt("id");
            DetallesProcesador(Id);
            ID.setText("ID: "+Id);
            nombre.setText("Nombre: "+bundle.getString("nombre"));
            nucleos.setText("Nucleos: "+bundle.getString("nucleos"));
            frecuencia.setText("Frecuencia: "+bundle.getString("fre"));
        } else {
            Toast.makeText(this, "Ocurrio un error al obtener los datos", Toast.LENGTH_LONG).show();
            startActivity(new Intent(Detalles__Procesador.this, Dashboard.class));
            finish();
        }
    }

    public void DetallesProcesador(int id) {
        servicio.DetallesProcesador(id).enqueue(new Callback<Peticion_Procesadores>() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onResponse(Call<Peticion_Procesadores> call, Response<Peticion_Procesadores> response) {
                if (response.isSuccessful()) {
                    if (response.body().getEstado()) {
                        List<Peticion_Procesadores.Procesador> detalles = response.body().getProcesadores();
                        for(Peticion_Procesadores.Procesador row: detalles){
                            ID.setText(String.format("%d", row.getId()));
                            nom = row.getNombre();
                            nucl = row.getNucleos();
                            fre = row.getFrecuencia();
                            nombre.setText(row.getNombre());
                            nucleos.setText(row.getNucleos());
                            frecuencia.setText(row.getFrecuencia());
                        }
                    } else {
                        Toast.makeText(Detalles__Procesador.this, "No se encuentra un usuario con esa informacion", Toast.LENGTH_LONG).show();
                    }
                } else
                    Toast.makeText(Detalles__Procesador.this, "Ocurrio un error al obtener los datos del procesador", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Peticion_Procesadores> call, Throwable t) {
                Log.e("Error API: ", "El error fue causado por: " + t.getCause());
                Toast.makeText(Detalles__Procesador.this, "Ocurrio un error al conectarnos a nuestros servidores", Toast.LENGTH_LONG).show();
            }
        });
    }
}
