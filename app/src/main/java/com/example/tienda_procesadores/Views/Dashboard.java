package com.example.tienda_procesadores.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tienda_procesadores.API.API;
import com.example.tienda_procesadores.MainActivity;
import com.example.tienda_procesadores.Models.AdaptadorProcesadores;
import com.example.tienda_procesadores.Models.Peticion_Procesadores;
import com.example.tienda_procesadores.R;
import com.example.tienda_procesadores.Services.ServicioAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity {

    RecyclerView rvLista;
    AdaptadorProcesadores adaptadorProcesadores;
    ServicioAPI servicio;
    Button btnAdios, btnRecargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        btnAdios = findViewById(R.id.btnSalir);
        btnRecargar = findViewById(R.id.btnRecargar);
        rvLista = findViewById(R.id.rvLista);
        rvLista.setHasFixedSize(true);
        rvLista.setLayoutManager(new LinearLayoutManager(Dashboard.this));
        rvLista.addItemDecoration(new DividerItemDecoration(Dashboard.this, DividerItemDecoration.VERTICAL));
        adaptadorProcesadores = new AdaptadorProcesadores(this);
        TodosLosProcesadores();
        btnAdios.setOnClickListener(v -> {
            BorrarPreferencias();
            startActivity(new Intent(Dashboard.this, MainActivity.class));
            finish();
        });
        btnRecargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TodosLosProcesadores();
            }
        });
    }

    public void TodosLosProcesadores(){
        servicio = API.getApi(Dashboard.this).create(ServicioAPI.class);
        servicio.TodosLosProcesadores("s").enqueue(new Callback<Peticion_Procesadores>() {
            @Override
            public void onResponse(Call<Peticion_Procesadores> call, Response<Peticion_Procesadores> response) {
                if (response.isSuccessful()) {
                    List<Peticion_Procesadores.Procesador> procesadores = response.body().getProcesadores();
                    adaptadorProcesadores.GuardarProcesadores(procesadores);
                    rvLista.setAdapter(adaptadorProcesadores);
                } else {
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(Dashboard.this, "A Ocurrido un error Obtener los Procesadores", Toast.LENGTH_LONG).show();
                    TodosLosProcesadores();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Procesadores> call, Throwable t) {
                TodosLosProcesadores();
                Log.e("Api Error: ", t.getMessage() +" "+ t.getCause());
                Toast.makeText(Dashboard.this, "A Ocurrido Al conectarse con el servidor", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void BorrarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN", "");
        editor.putString("TOKEN_TEMP", "");
        editor.commit();
    }

    @Override
    public void onDestroy(){
     super.onDestroy();
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN_TEMP", "");
        editor.commit();
    }
}
