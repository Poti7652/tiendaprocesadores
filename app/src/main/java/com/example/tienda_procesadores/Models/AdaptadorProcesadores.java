package com.example.tienda_procesadores.Models;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tienda_procesadores.R;
import com.example.tienda_procesadores.Views.Dashboard;
import com.example.tienda_procesadores.Views.Detalles__Procesador;

import java.util.List;

public class AdaptadorProcesadores extends RecyclerView.Adapter<AdaptadorProcesadores.ViewHolder>{

    List<Peticion_Procesadores.Procesador> lista;
    Context context;
    public AdaptadorProcesadores(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_procesador, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Peticion_Procesadores.Procesador procesador = lista.get(position);
        holder.texto.setText(procesador.getNombre());
        holder.btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Detalles__Procesador.class);
                intent.putExtra("id", lista.get(position).getId());
                intent.putExtra("nombre", lista.get(position).getNombre());
                intent.putExtra("nucleos", lista.get(position).getNucleos());
                intent.putExtra("fre", lista.get(position).getFrecuencia());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void GuardarProcesadores(List<Peticion_Procesadores.Procesador> lista){
        this.lista = lista;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView texto;
        ImageButton btnImage;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            texto = itemView.findViewById(R.id.txtvTexto);
            btnImage = (ImageButton) itemView.findViewById(R.id.ibtnDetalles);
        }
    }

}
