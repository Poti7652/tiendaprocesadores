package com.example.tienda_procesadores.Models;

import java.util.ArrayList;
import java.util.List;

public class Peticion_DetalleProcesadores {
    boolean estado;
    List<Peticion_Procesadores.Procesador> procesador = new ArrayList<>();

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public List<Peticion_Procesadores.Procesador> getProcesador() {
        return procesador;
    }

    public void setProcesador(List<Peticion_Procesadores.Procesador> procesador) {
        this.procesador = procesador;
    }
}
