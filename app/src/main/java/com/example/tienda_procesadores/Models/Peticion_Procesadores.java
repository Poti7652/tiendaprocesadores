package com.example.tienda_procesadores.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Peticion_Procesadores implements Serializable{
    private boolean estado;
    private List<Procesador> procesadores = new ArrayList<>();

    public class Procesador implements Serializable{
        int id;
        String nombre;
        String nucleos;
        String frecuencia;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getNucleos() {
            return nucleos;
        }

        public void setNucleos(String nucleos) {
            this.nucleos = nucleos;
        }

        public String getFrecuencia() {
            return frecuencia;
        }

        public void setFrecuencia(String frecuencia) {
            this.frecuencia = frecuencia;
        }
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public List<Procesador> getProcesadores() {
        return procesadores;
    }

    public void setProcesadores(List<Procesador> procesadores) {
        this.procesadores = procesadores;
    }
}
